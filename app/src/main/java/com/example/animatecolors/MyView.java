package com.example.animatecolors;

import android.content.Context;
import android.widget.LinearLayout;

import static com.example.animatecolors.MainActivity.*;

/**
 * Created by lynn on 3/15/2015.
 */
public class MyView extends LinearLayout {


    public MyView(Context context) {
        super(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(400,400);

        myCanvas.setLayoutParams(layoutParams);

        addView(myCanvas);

    }


}
