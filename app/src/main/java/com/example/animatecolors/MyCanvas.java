package com.example.animatecolors;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by lynn on 3/15/2015.
 */
public class MyCanvas extends View {

    public MyCanvas(Context context) {
        super(context);
    }

    private Paint generatePaint() {
        int red = (int)(256*Math.random());
        int green = (int)(256*Math.random());
        int blue = (int)(256*Math.random());

        Paint paint = new Paint();

        paint.setARGB(255,red,green,blue);

        return(paint);
    }

    public void draw() {
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        Paint paint = generatePaint();

        canvas.drawRect(0,0,100,100,paint);

        paint = generatePaint();

        canvas.drawRect(100,0,200,100,paint);

        paint = generatePaint();

        canvas.drawRect(0,100,100,200,paint);

        paint = generatePaint();

        canvas.drawRect(100,100,200,200,paint);
    }

}
