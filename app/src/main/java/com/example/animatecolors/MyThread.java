package com.example.animatecolors;

import static com.example.animatecolors.MainActivity.*;

/**
 * Created by lynn on 3/15/2015.
 */
public class MyThread implements Runnable {
    private Thread thread;
    private boolean keepGoing;

    public MyThread() {
        keepGoing = true;

        thread = new Thread(this);

        thread.start();
    }

    public void stop() {
        keepGoing = false;
    }

    public void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    public void run() {
        while (keepGoing) {
            myCanvas.post(new Runnable() {

                public void run() {
                    myCanvas.draw();
                }
            });


            pause(0.5);
        }
    }

}
